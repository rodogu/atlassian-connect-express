// modified from https://bitbucket.org/knecht_andreas/atlassian-oauth-validator
var _ = require('underscore');
var urls = require('url');
var oauth = require('../internal/oauth');
var hostInfo = require('../internal/host-info');

module.exports = function (addon, optionalPublicKey) {
  var maxTimestampAge = 5 * 60 * 1000;
  var usedNonces = [];

  function expireOldNonces(currentTime) {
    var min = (currentTime - maxTimestampAge + 500) / 1000;
    usedNonces = _.filter(usedNonces, function (item) {
      return item.time > min;
    });
  }

  return function (req, res, next) {
    addon.emit('oauth_verification_triggered');
    // allows disabling of oauth for testing/debugging
    if (/no-oauth/.test(process.env.AC_OPTS)) return next();
    var authHeader = req.get('authorization');
    var params, clientKey, version, nonce, method, timestamp, signature;

    if (authHeader && authHeader.indexOf('OAuth ') === 0) {
      params = _.object(authHeader.slice(6)
        .split(',')
        .filter(function (s) {
          return !!s;
        })
        .map(function (s) {
          var nv = s.split('=');
          var n = nv[0].trim();
          var v = nv[1].trim().slice(1, nv[1].length - 1);
          return [n, decodeURIComponent(v)];
        })
      );
      params = _.extend(params, req.query);
    }
    else {
      params = req.query || {};
    }

    clientKey = params['oauth_consumer_key'];
    version = params['oauth_version'];
    nonce = params['oauth_nonce'];
    method = params['oauth_signature_method'];
    timestamp = params['oauth_timestamp'];
    signature = params['oauth_signature'];

    function send(code, msg) {
      addon.logger.error('OAuth verification error:', code, msg);
      if (addon.config.errorPage) {
        res.redirect(addon.config.errorPage() + "?status=" + code + "&message=" + encodeURIComponent(msg) +
          "&oauth_consumer_key=" + params.oauth_consumer_key +
          "&xdm_e=" + params.xdm_e + '&xdm_c=' + params.xdm_c + "&cp=" + params.cp);
      } else {
        res.send(code,msg);
      }
    }

    if (!version || isNaN(version) || version > 1.0) {
      return send(400, 'Invalid oauth version specified: ' + version);
    }

    if (!timestamp || isNaN(timestamp)) {
      return send(400, 'Invalid oauth timestamp specified: ' + timestamp);
    }

    if (!nonce) {
      return send(400, 'Invalid oauth nonce specified: ' + nonce);
    }

    var alreadyUsed = _.any(usedNonces, function (item) {
      return item.nonce == nonce;
    });

    if (alreadyUsed) {
      return send(401, 'OAuth nonce already used.');
    }

    if (addon.app.get('env') !== 'development') {
      var now = Date.now();
      usedNonces.push({nonce: nonce, time: now});

      expireOldNonces(now);

      var min = (now - maxTimestampAge + 500) / 1000;
      var max = (now + maxTimestampAge + 500) / 1000;

      if (timestamp < min || timestamp > max) {
        return send(401, 'OAuth timestamp refused.');
      }
    }

    var path = req.originalUrl;
    var qIndex = path.indexOf('?');
    if (qIndex >= 0) path = path.slice(0, qIndex);
    var url = urls.parse(addon.config.localBaseUrl());
    url.pathname = path;
    url = urls.format(url);

    function verify(publicKey,success) {
      oauth.verify({
        method: req.method,
        url: url,
        query: params,
        publicKey: publicKey,
        signature: signature,
        signatureMethod: method,
        logger: addon.logger
      }, function (err) {
        if (err) {
          send(401, 'OAuth request not authenticated: ' + err.message)
        }
        else {
          addon.emit('oauth_verification_successful');
          next();
          if (success) {
            success();
          }
        }
      });
    }

    function failNotApproved() {
      send(401, 'OAuth consumer ' + clientKey + ' not approved to make requests.');
    }

    function lookupAndVerify() {
      addon.emit('oauth_verification_lookup_host',clientKey,req);
      var baseUrl;
      if (req.headers && req.headers.xdm_e) {
        baseUrl = req.headers.xdm_e + req.headers.cp || '';
      } else if (req.query && req.query.xdm_e) {
        baseUrl = req.query.xdm_e + req.query.cp || '';
      }
      if (baseUrl) {
        // let's check if remote host is even allowed
        var host = urls.parse(baseUrl).hostname;
        var whitelisted = addon.config.whitelist().some(function (re) { return re.test(host); });
        if (whitelisted) {
          hostInfo.get(baseUrl).then(function(info) {
            if (info) {
              verify(info.publicKey,function() {
                var clientInfo  = {
                  baseUrl: baseUrl,
                  clientKey: clientKey,
                  description: info.description,
                  key: addon.key,
                  publicKey: info.publicKey
                };
                addon.emit('remote_plugin_enabled',clientKey,clientInfo);
              });
            } else {
              failNotApproved();
            }
          },failNotApproved);
        } else {
          failNotApproved();
        }
      } else {
        failNotApproved();
      }
    }
    if (optionalPublicKey) {
      verify(optionalPublicKey);
    }
    else {
      addon.settings.get('clientInfo', clientKey).then(
        function (consumer) {
          if (!consumer) {
            if (/enable-on-request/.test(process.env.AC_OPTS)) {
              lookupAndVerify();
            } else {
              failNotApproved();
            }
          }
          else {
            verify(consumer.publicKey);
          }
        }).fail(function (err) {
          send(401, 'OAuth request not authenticated due to consumer lookup failure: ' + err)
        }
      );
    }

  };

};
